
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class HomeWork1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random random = new Random();

        // ask player her/his name
        System.out.println("Please enter your name: ");

        String name = in.next();

        System.out.println("Let's the game begin, " + name + " !");
        int ar [] = new int [2];
        int index = 0;
        // Generate random number between 1 and 100
        int main_number = random.nextInt(99)+1;
        System.out.println("Guess the number");
        // to see our random number ( to check the homework, you can un comment next line
        //  System.out.println(main_number);
        boolean flag = false;
        // to get number form console
        int num = getNumber(flag);
        flag = true;
        // to check the console number equals the main number or not
        while(num != main_number){
            // add number to array
            ar = addNumberToArray(ar, index, num);
            index ++;
            flag = false;
            printMessage(num, main_number);
            // to get number form console
            num = getNumber(flag);
            // to check the console number equals the main number or not
            if(num ==main_number) {
                flag = true;
                // add number to array
                ar = addNumberToArray(ar, index, num);
                index ++;
            }
        }
        System.out.println("Congratulations," + name +"  Your number is : " + main_number);

        ar = sortArray(ar);
        // print all numbers
        System.out.println(Arrays.toString(ar));

    }

    // print message number is big or small
    public static void printMessage(int num, int main_number){
        if(num>main_number){
            System.out.println("Your number is too big. Please, try again. ");
        }
        else if(num<main_number) System.out.println("Your number is too small. Please, try again. ");
    }
    // to check input is number or not
    public static boolean checkNumber(String a){
        for (int i= 0; i<a.length(); i++){
            if(a.charAt(i)<48 || a.charAt(i)>57){
                return false;
            }
        }
        return true;
    }
    // to get number from console
    public static int getNumber(boolean flag){
        Scanner in = new Scanner(System.in);
        String number = "";
        while (flag == false) {
            System.out.println("Please enter a number: ");
            number = in.next();
            flag = checkNumber(number);
        }
        return Integer.parseInt(number);
    }

    // to inlarge array
    public static int[] inlargeArray(int[] ar, int index ) {
        if (index == ar.length) {
            int[] ar2 = new int[ar.length +1];
            ar2 = Arrays.copyOf(ar, ar.length +1);


            return ar2;
        } else return ar;
    }
    // to add number to array
    public static int [] addNumberToArray(int []ar, int index, int a){
        ar = inlargeArray(ar, index);
        ar[index] = a;
        return ar;
    }


    // sort all numbers
    public static int [] sortArray(int []ar){
        for(int i = 0; i<ar.length-1; i++){
            for (int j = 0; j<ar.length-1-i; j++ ){
                if(ar[j]<ar[j+1]){
                    int temp = ar[j];
                    ar[j] = ar[j+1];
                    ar[j+1] = temp;
                }
            }
        }
        return ar;
    }
}
